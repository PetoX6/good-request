package com.example.goodrequest.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.goodrequest.R
import com.example.goodrequest.model.User
import com.example.goodrequest.model.UserResponse
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_detail.*

class UserDetailFragment : BaseFragment(){

    companion object {
        const val tag  = "com.example.goodrequest.UserDetailFragmentTag"
    }

    private var userId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        userId = arguments!!.getInt(UserDetailFragment.tag)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initLayout()
        subscribe(userViewModel.getUser(userId))
    }

    private fun subscribe(observable: Observable<UserResponse>) {
        observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: Observer<UserResponse> {
                override fun onNext(singleUserResponse: UserResponse) {
                    val user: User = singleUserResponse.user
                    nameTextView.text = user.first_name + " " + user.last_name
                    idTextView.text = user.id.toString()
                    Glide.with(this@UserDetailFragment).load(user.avatarUrl).apply(RequestOptions.circleCropTransform()).into(avatarImageView)
                }
                override fun onSubscribe(d: Disposable) {
                    showSwipeRefresh()
                }
                override fun onComplete() {
                    hideSwipeRefresh()
                }
                override fun onError(e: Throwable) {
                    hideSwipeRefresh()
                    showErrorSnackbar()
                }
            })
    }

    private fun initLayout(){
        (activity as AppCompatActivity).setSupportActionBar((toolbar as Toolbar))
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (toolbar as Toolbar).setTitle(R.string.user_detail_fragment_title)
        (toolbar as Toolbar).setNavigationOnClickListener {
            fragmentManager!!.popBackStack()
        }

        swipeRefreshLayout.setOnRefreshListener{
            subscribe(userViewModel.getUser(userId))
        }
    }

    override fun showErrorSnackbar() {
        Snackbar.make(view!!, getText(R.string.error_text), Snackbar.LENGTH_INDEFINITE)
            .setActionTextColor(Color.WHITE)
            .setAction(getText(R.string.again)) {
                subscribe(userViewModel.getUser(userId))
            }
            .show()
    }
}