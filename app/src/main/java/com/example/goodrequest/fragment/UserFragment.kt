package com.example.goodrequest.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.GridLayoutManager
import com.example.goodrequest.R
import com.example.goodrequest.model.User
import com.example.goodrequest.model.UserListResponse
import com.example.goodrequest.ui.OnBottomReachedListener
import com.example.goodrequest.ui.UserRecyclerViewAdapter
import com.google.android.material.snackbar.Snackbar
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user.*

class UserFragment: BaseFragment() {

    private var pageCounter: Int = 1
    private var users: MutableList<User> = mutableListOf()
    private var maxUsers: Int = 0
    private lateinit var userAdapter: UserRecyclerViewAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initLayout()
        subscribe(userViewModel.getUsers(pageCounter))
    }

    private fun subscribe(observable: Observable<UserListResponse>) {
        observable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: Observer<UserListResponse> {
                override fun onNext(userListResponse: UserListResponse) {
                    maxUsers = userListResponse.total
                    users.addAll(userListResponse.users)
                    userAdapter.notifyDataSetChanged()
                }
                override fun onSubscribe(d: Disposable) {
                    showSwipeRefresh()
                }
                override fun onComplete() {
                    hideSwipeRefresh()
                    pageCounter++
                }
                override fun onError(e: Throwable) {
                    hideSwipeRefresh()
                    showErrorSnackbar()
                }
            })
    }

    private fun initLayout(){
        (activity as AppCompatActivity).setSupportActionBar((toolbar as Toolbar))
        (toolbar as Toolbar).setTitle(R.string.user_fragment_title)

        userAdapter = UserRecyclerViewAdapter(users, context!!)
        recyclerView.layoutManager = GridLayoutManager(context!!, 2)
        recyclerView.adapter = userAdapter

        userAdapter.setOnBottomReachedListener(object: OnBottomReachedListener {
            override fun onBottomReached() {
                if(users.size < maxUsers) subscribe(userViewModel.getUsers(pageCounter))
            }
        })

        swipeRefreshLayout.setOnRefreshListener{
            pageCounter = 1
            users.removeAll { true }
            subscribe(userViewModel.getUsers(pageCounter))
        }
    }

    override fun showErrorSnackbar() {
        Snackbar.make(view!!, getText(R.string.error_text), Snackbar.LENGTH_INDEFINITE)
            .setActionTextColor(Color.WHITE)
            .setAction(getText(R.string.again)) {
                subscribe(userViewModel.getUsers(pageCounter))
            }
            .show()
    }
}