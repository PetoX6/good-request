package com.example.goodrequest.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.goodrequest.viewmodel.UserViewModel
import kotlinx.android.synthetic.main.fragment_user.*

abstract class BaseFragment : Fragment() {

    protected lateinit var userViewModel: UserViewModel

    protected abstract fun showErrorSnackbar()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userViewModel = ViewModelProviders.of(activity!!).get(UserViewModel::class.java)
    }

    protected fun showSwipeRefresh(){
        if(swipeRefreshLayout != null && !swipeRefreshLayout.isRefreshing) {
            swipeRefreshLayout.isRefreshing = true
        }
    }

    protected fun hideSwipeRefresh(){
        if(swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing) {
            swipeRefreshLayout.isRefreshing = false
        }
    }
}