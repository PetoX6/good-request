package com.example.goodrequest.viewmodel

import androidx.lifecycle.ViewModel
import com.example.goodrequest.api.Api
import com.example.goodrequest.api.ApiService
import com.example.goodrequest.model.UserResponse
import com.example.goodrequest.model.UserListResponse
import io.reactivex.Observable

class UserViewModel : ViewModel() {

    fun getUsers(page: Int): Observable<UserListResponse> {
        return ApiService.getClient().create(Api::class.java).getUsers(page)
    }

    fun getUser(id: Int): Observable<UserResponse> {
        return ApiService.getClient().create(Api::class.java).getUser(id)
    }
}