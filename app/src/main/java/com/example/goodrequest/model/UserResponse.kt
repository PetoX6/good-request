package com.example.goodrequest.model

import com.google.gson.annotations.SerializedName

class UserResponse (
    @SerializedName("data") val user: User
)