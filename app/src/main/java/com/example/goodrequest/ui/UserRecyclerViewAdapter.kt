package com.example.goodrequest.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.goodrequest.R
import com.example.goodrequest.fragment.UserDetailFragment
import com.example.goodrequest.model.User
import kotlinx.android.synthetic.main.user_list_item.view.*

class UserRecyclerViewAdapter(private val users: List<User>, private val context: Context) : RecyclerView.Adapter<UserRecyclerViewAdapter.UserViewHolder>()  {

    private lateinit var onBottomReachedListener: OnBottomReachedListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        val inflatedView = LayoutInflater.from(context).inflate(R.layout.user_list_item, parent, false)
        return UserViewHolder(inflatedView)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        if(position == users.size - 1){
            onBottomReachedListener.onBottomReached()
        }
        val user: User = users[position]
        holder.bindUser(user)
        holder.itemView.setOnClickListener(ItemOnClickListener(user))
    }

    fun setOnBottomReachedListener(listener: OnBottomReachedListener){
        onBottomReachedListener = listener
    }

    inner class UserViewHolder(view: View) : RecyclerView.ViewHolder(view){
        fun bindUser(user: User) {
            itemView.idTextView.text = user.id.toString()
            itemView.nameTextView.text = user.first_name + " " + user.last_name
            Glide.with(context).load(user.avatarUrl).apply(RequestOptions.circleCropTransform()).into(itemView.avatarImageView)
        }
    }

    inner class ItemOnClickListener(private val user: User): View.OnClickListener{
        override fun onClick(v: View?) {
            val ft: FragmentTransaction = (context as AppCompatActivity).supportFragmentManager.beginTransaction()
            val fragment = UserDetailFragment()
            val bundle = Bundle()
            bundle.putInt(UserDetailFragment.tag, user.id)
            fragment.arguments = bundle
            ft.replace(R.id.fragmentContainer, fragment)
            ft.addToBackStack(null)
            ft.commit()
        }
    }
}