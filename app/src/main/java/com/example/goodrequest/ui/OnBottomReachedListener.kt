package com.example.goodrequest.ui

interface OnBottomReachedListener {
    fun onBottomReached()
}