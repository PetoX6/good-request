package com.example.goodrequest.api

import com.example.goodrequest.model.UserListResponse
import com.example.goodrequest.model.UserResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("users")
    fun getUsers(
        @Query("page") page: Int,
        @Query("per_page") per_page: Int = 5): Observable<UserListResponse>

    @GET("users")
    fun getUser(
        @Query("id") id: Int): Observable<UserResponse>
}